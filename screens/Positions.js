import React, { useState, useEffect } from 'react'
import { Text, View, Button, FlatList } from 'react-native'

import globalStyles from '../assets/css/styles'
import Housing from '../components/global/Housing'
import PositionItem from '../components/positions/PositionItem'
import PositionsSummary from '../components/positions/PositionsSummary'
import { get_latest_prices, get_latest_price } from '../external/Oanda_api'

import db from '../database/firebaseDB'
import { ref, onValue, set} from "firebase/database";

const PositionsScreen = () => {


	const [openTrades, setOpenTrades] = useState([])
	const [refreshing, setRefreshing] = useState(false)
	const tradesRef = ref(db, 'positions')

	const wait = (timeout) => {
		return new Promise(resolve => setTimeout(resolve, timeout));
	}

	const updatePrices = async () => {
	    price_requests = openTrades.map((position) => {
	    	return {
	    		instrument: position.instrument,
	    		type: position.long_short
	    	}
	    })
	    let prices = await get_latest_prices(price_requests)
	    let newOpenTrades = openTrades.map((position, i) => {
	    	position.current_price = prices[i]
	    	return position
	    })
	    newOpenTrades.forEach(updatePl)
	    setOpenTrades(newOpenTrades)
	}

	const updatePl = (position) => {
		position.pl = (position.current_price - position.open_price) * position.amount
	}

	const onRefresh = async () => {
	    setRefreshing(true);
	    await updatePrices()
	    setRefreshing(false);
	}

	useEffect(() => {
		onValue(tradesRef, (snapshot) => {
			let allTrades = Object.values(snapshot.val())
			allTrades.reverse()

			let openTrades = allTrades.filter((trades) => {
				return trades.open
			})

			setOpenTrades(openTrades)
		})
	}, [])

	return (
		<Housing>
			{openTrades.length == 0 &&
				<View style={{ height: '100%', justifyContent: 'center' }}>
					<Text style={ globalStyles.big }>No open positions</Text>
				</View>
			}
			{openTrades.length > 0 &&
				<View style={{ width: '100%', height: '100%' }}>
					<PositionsSummary
						positions={openTrades}
					/>
					<FlatList
						style={{width: '100%'}}
						data={openTrades}
						renderItem={({ item } ) => (
							<PositionItem
								instrument={item.instrument}
								pl={item.pl}
								trade_units={item.amount}
								long_short={item.long_short}
								open_price={item.open_price}
								open_datetime={item.datetime_open}
								current_price={item.current_price}
							/>
						)}
						keyExtractor={(item, index) => index.toString()}
						onRefresh={onRefresh}
						refreshing={refreshing}
					/>
				</View>
			}
		</Housing>
	)
}

export default PositionsScreen;