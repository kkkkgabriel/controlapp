import React from 'react'
import { Text, View, StyleSheet } from 'react-native'
import globalStyles from '../../assets/css/styles'

const PositionsSummary = ({ positions }) => {
	const pl = positions.reduce((v, trade) => { return trade.pl + v }, 0)
	return (
		<View style={styles.container}>
			{!isNaN(pl) &&
				<View>
					<View style={styles.row}>
						<Text style={globalStyles.big}>Unrealised P&L: </Text>
						<Text style={pl > 0 ? {...globalStyles.big, color: 'limegreen'} : {...globalStyles.big, color: 'red'}}>{pl.toFixed(2)}</Text>
					</View>
					<View style={styles.row}>
						<Text>{positions.length} positions</Text>
					</View>
				</View>
			}
			{isNaN(pl) &&
				<View style={styles.row}>
					<Text style={globalStyles.big}>Pull to fetch prices</Text>
				</View>
			}
		</View>
	)
}
const styles = StyleSheet.create({
	container: {
		padding: 10,
		borderBottomWidth: 1,
		borderTopWidth: 1,
		borderColor: 'gainsboro',
		marginBottom: 20,
		width: '100%'
	},
	row: {
		width: '100%',
		justifyContent: 'center',
		flexDirection: 'row'
	}
})

export default PositionsSummary

