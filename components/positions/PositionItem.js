import React from 'react'
import { Text, View, StyleSheet } from 'react-native'
import globalStyles from '../../assets/css/styles'

import Ctime from '../../utils/ctime'

const PlPanel = ({ pl }) => {
	return (
		<View>
			{!isNaN(pl) &&
				<Text style={globalStyles.vbig}>{(Math.round(pl * 100) / 100).toFixed(2)}</Text>
			}
			{isNaN(pl) &&
				<Text style={globalStyles.big}>-</Text>
			}
		</View>
	)
}

const HeaderPanel = () => {
	return (
		<View>
			<Text>Instrument</Text>
			<Text>Trade units</Text>
			<Text>Type</Text>
			<Text>Open price</Text>
			<Text>Datetime of open</Text>
			<Text>Current price</Text>
		</View>
	)
}

const ContentPanel = ({
	instrument,
	trade_units,
	long_short,
	open_price,
	open_datetime,
	current_price
}) => {

	const display_open_datetime = new Ctime().from_datetime_string(open_datetime).to_display_time()
	return (
		<View>
			<Text>{instrument}</Text>
			<Text>{trade_units}</Text>
			<Text>{long_short}</Text>
			<Text>{open_price}</Text>
			<Text>{display_open_datetime}</Text>
			<Text>{current_price}</Text>
		</View>
	)
}

const PositionItem = ({
	instrument,
	pl,
	trade_units,
	long_short,
	open_price,
	open_datetime,
	current_price
}) => {
	let color = 'aquamarine'
	if (pl < 0) color = 'pink'
	if (isNaN(current_price)) current_price = '-'
	return (
		<View style={{...styles.container, 'backgroundColor': color}}>
			<View style={styles.pl}>
				<PlPanel 
					pl={pl}
				/>
			</View>
			<View style={styles.row}>
				<HeaderPanel />
				<ContentPanel		
					instrument={instrument}
					trade_units={trade_units}
					long_short={long_short}
					open_price={open_price}
					open_datetime={open_datetime}
					current_price={current_price}
				/>
			</View>
		</View>
	)
}

const styles = StyleSheet.create({
	container: {
		width: '100%',
		borderWidth: 1,
		borderColor: 'gainsboro',
		paddingTop: 10,
		paddingBottom: 20,
		borderRadius: 30,
		marginBottom: 10
	},
	row: {
		flexDirection: 'row',
		justifyContent: 'space-evenly',
	},
	pl: {
		flexDirection: 'row',
		justifyContent: 'center'
	}
})

export default PositionItem