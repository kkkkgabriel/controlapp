# Overview
This app is build to complement [trading bot](https://bitbucket.org/kkkkgabriel/tradingbot). This app allows me to view the trades across dates, open positions, account statuses, and manage control bits of the trading bot. These features are implemented across the tabs of the app as follows:    
- [**Trades:**](#trades) In this tab, we can viewing trades details, such as open and close prices, instrument, datetime of open and close, and profit loss. The trades viewed on this tab depends on the date selected    
- [**Positions:**](#positions) In this tab, we can view open positions, with live prices according to a selected platform.        
- [**Controls:**](#controls) Control features are implemented here. The 'open' button toggles the system's 'open' bit. If set to false, the system will stop opening positions. The 'close' button toggles the system's 'close' bit. If set to true, all open positions be closed on the next run.    
- [**Accounts:**](#accounts) Graphical visualisations of the account status, including account balance, profit loss, margin available and margin used.    
For each of the above tabs, see corresponding screenshots for example. 

# Code structure    
* [App.js](App.js) contains the root script that implement the root of the app. This root implements a tab navigation that contains the four screens: trades, positions, controls, accounts.    
* [Screens](screens/) contains the scripts for each of the screens. They implement components that are written in [components](components/).    
* [Components](components/) contains partial elements of the screens. The folder contains subfolders for each of the screens, and a global folder for components that a used across all of the screens.    
* [Utils/ctime.js](utils/ctime.js) implements the ctime (short for custom time) class to manage time objects.    
* [Database](database/) contains the necessary configurations for the database (firebase) connection. An example js script has been included.    
* [redux](redux/) is not implemented as of now, perhaps will be used in future implementation.      
* External (not pushed to this remote repository) contains codes for the connection to the platform API. This folder contains some sensitive files, thus are not pushed. Nevertheless, this folder is required to make the app work.     

# Screenshots
## Trades
![screenshot of datepicker in trades screen](assets/docs/trades_datepicker.jpg)&nbsp;
![screenshot of trades screen](assets/docs/trades.jpg)

## Positions
![screenshot of positions screen](assets/docs/positions.jpg)

## Controls
![screenshot of controls screen](assets/docs/controls.jpg)

## Accounts
![screenshot of accounts screen](assets/docs/accounts.jpg)
